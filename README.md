## add_cluster_to_vault.yaml
* This repository contains a role related to HashiCorp Vault.
* Prerequisites
	* `OC` installed on the server.
	*  `Ansible` installed on the server.
	* `ansible-modules-hashivault` Python module installed on the server.

 * The playbook is used when we would like to add a cluster to the HashiCorp Vault.
 * The playbook creates: 
   * ServiceAccount `vault-auth-delegator` in `kube-system` namespace
   * ClusterRoleBinding `vault-auth-delegator` 
   * A Kubernetes auth method in Vault

| Variable | Description | Type | Default | Required |
| ------------- |:-----------------:| -----:|:-------------:| -----:|
| vault_url    | URL of the Vault | String | "" | True
| vault_username     | Username to the Vault      |   String | "" | True
| vault_password | Password to the Vault      |    String | "" | True
| cluster_name | Name of the cluster      |    String | "" | True
| base_domain | The base domain of the cluster      |    String | "" | True
| client_name| Name of the cluster      |    String | "" | True